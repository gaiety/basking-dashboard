# basking-dashboard

Dashboard for my personal home server

## Using on Ubuntu Linix

```bash
npm i
npm run mk:data # or ./build.sh
```

## Developing

```bash
npm i
npm run mk:data:test
```
