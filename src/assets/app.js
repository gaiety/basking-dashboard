new Vue({
  el: '#app',
  data: {
    fetchingLastBuildDate: true,
    lastBuildDate: '',

    fetchingDiskSpace: true,
    diskSize: '',
    diskUsed: '',
    diskCapacity: '',

    fetchingCPUUsage: true,
    cpuUsageNum: 0,
    cpuUsageStr: '',

    fetchingUptime: true,
    uptime: '',

    time: '',
    date: '',
  },
  created() {
    this.fetchStats(true);
    this.updateTimeAndDate();
		setInterval(this.updateTimeAndDate, 1000); // 1 Second
		setInterval(this.fetchStats, 600000); // 10 Minutes
  },
  methods: {
    fetchStats(showAsFetching = false) {
      this.fetchLastBuildDate(showAsFetching);
      this.fetchDiskSpace(showAsFetching);
      this.fetchCPUUsage(showAsFetching);
      this.fetchUptime(showAsFetching);
    },
    updateTimeAndDate() {
      let currentTime = moment();
      this.time = currentTime.format('h:mm');
      this.date = currentTime.format('dddd, MMMM Do');
    },
    async fetchLastBuildDate(showAsFetching) {
      this.fetchingLastBuildDate = showAsFetching;
      try {
        let { data } = await axios.get('/api/builddate');
        this.lastBuildDate = data.pretty;
        this.fetchingLastBuildDate = false;
      } catch (err) { throw new Error(err); }
    },
    async fetchDiskSpace(showAsFetching) {
      this.fetchingDiskSpace = showAsFetching;
      try {
        let { data } = await axios.get('/api/diskspace');
        this.diskSize = data.size;
        this.diskUsed = data.used;
        this.diskCapacity = data.capacity;
        this.fetchingDiskSpace = false;
      } catch (err) { throw new Error(err); }
    },
    async fetchCPUUsage(showAsFetching) {
      this.fetchingCPUUsage = showAsFetching;
      try {
        let { data } = await axios.get('/api/cpuusage');
        this.cpuUsageNum = data.num;
        this.cpuUsageStr = data.str;
        this.fetchingCPUUsage = false;
      } catch (err) { throw new Error(err); }
    },
    async fetchUptime(showAsFetching) {
      this.fetchingUptime = showAsFetching;
      try {
        let { data } = await axios.get('/api/uptime');
        this.uptime = moment(data.uptime, 'HH:MM MM-DD-YYYY').fromNow();
        this.fetchingUptime = false;
      } catch (err) { throw new Error(err); }
    },
  },
});
